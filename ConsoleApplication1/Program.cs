﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;
using TEmit;
namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int rows = 10000;
            DataTable dt = DataTableData.GetCustomer(rows);
            Stopwatch sw = new Stopwatch();
            sw.Restart();

            List<TEmit.Customer> list = DataTableConvert.ToList<Customer>(dt);
            sw.Stop();
            Console.WriteLine("{0} ,generate  TEmit.EntityConverter.ToList, total {1} rows, time: {2}", "Assign".PadRight(12, ' '), rows, sw.ElapsedMilliseconds.ToString());
           
            
             sw.Restart();
             list = ToEntityByEmit.GetList<Customer>(dt);
          sw.Stop();
          Console.WriteLine("{0} ,generate  TEmit.EntityConverter.ToList, total {1} rows, time: {2}", "Assign".PadRight(12, ' '), rows, sw.ElapsedMilliseconds.ToString());
          //ModelHelper

          sw.Restart();
         // list = ModelHelper<Customer>.ConvertModel(dt);
          sw.Stop();
          Console.WriteLine("{0} ,generate  TEmit.EntityConverter.ToList, total {1} rows, time: {2}", "Assign".PadRight(12, ' '), rows, sw.ElapsedMilliseconds.ToString());
          
            Console.ReadLine();
        }
    }
}
