using System;

namespace DotNetScriptEngine
{	
	public delegate void TraceEventHandler(object sender, TraceEventArgs e);

	public class TraceEventArgs : EventArgs
	{
		string trace;
		Exception ex;

		private TraceEventArgs()
		{
		}

		public TraceEventArgs (string trace, Exception ex) : base()
		{
			this.trace = trace;
			this.ex = ex;
		}

		public string Trace
		{
			get {return trace;}
		}

		public Exception Ex
		{
			get {return ex;}
		}

	}
}
