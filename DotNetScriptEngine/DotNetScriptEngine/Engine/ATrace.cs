using System;

namespace DotNetScriptEngine
{
	public abstract class ATrace
	{
		private readonly object eventLock = new object();


		public ATrace()
		{
		}


		public void OnTrace (string trace, Exception ex)
		{
			TraceEventHandler handler;

			lock (eventLock)
			{
				handler = traceEvent;
			}
			if (handler != null)
			{
				handler (this, new TraceEventArgs(trace, ex));
			}
		}

		public event TraceEventHandler Trace
		{
			add
			{
				lock (eventLock)
				{
					traceEvent += value;
				}
			}
			remove
			{
				lock (eventLock)
				{
					traceEvent -= value;
				}
			}
		}
		private TraceEventHandler traceEvent;


	}
}
