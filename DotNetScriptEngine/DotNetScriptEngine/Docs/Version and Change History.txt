version 2.0.0.1
This version was so large and so much changed that I figured it warented a major version number increase.
The following is a list of fixes and enhancments

An app config file was added in order to define referenced assemblies, additional languages that
you can use to write Dot Net Script in, and user preferences.  The values defined in the config file act
line the machine config.  These are base values, but can be overridden by the values set in the dnml file.
The values in the dnml file have the highest priority and will be used by the script engine.  But you can 
use the settings in the config file to not have any settings in the dnml file, only script.

User preferences config section:
	A user preferences section has been added to the app.config file.  This section defines three
settings.  The default language, the script entry point, and the wait for user action flag.
The default language can be used to set what language the script will be compiled for if the 
language element is not in the dnml file.  The entry point is the method that the script engine
will call if the dnml file does not define an entry point.  The waitForUserAction flag is a bool
that determines if the console window will stay open and wait for a crlf when the script is finished 
running.  If this is not defined in the dnml file, the value in the config file will be used by
the script engine.  Below is an example of this section.

<userPreferences defaultLanguage="C#" entryPoint="Main" waitForUserAction="true" />	


Referenced Assemblies config section:
	This section lets you define assemblies that are required for the script to run.  Any assembly
defined in this section will be compiled into every script that is run.  Just the assembly name is
required, not the full assembly path.  Below is an example of this section.

<referencedAssemblies>
	<assembly path="System.dll" />		
	<assembly path="System.Messaging.dll" />		
	<assembly path="System.Security.dll" />		
</referencedAssemblies>


Supported Languages config section
	This section lets you dynamicly add new languages to the script engine without having to recompile
the engine.  the name attribute is the name that is entered in the dnml file or the defaultLanguage 
attribute in the user preferences config section.  The assembly attribute is the full path and file
name of the assembly that contains the language's implementation of the code provider.  The 
codeProviderName attribute is the name, including namespace of the language's code provider class.

<supportedLanguages>		
	<language name="JScript" assembly="C:\WINNT\Microsoft.NET\Framework\v1.1.4322\Microsoft.JScript.dll" 
		codeProviderName="Microsoft.JScript.JScriptCodeProvider" />
	<language name="J#" assembly="c:\winnt\microsoft.net\framework\v1.1.4322\vjsharpcodeprovider.dll" 
		codeProviderName="Microsoft.VJSharp.VJSharpCodeProvider" />
</supportedLanguages>



Version 1.0.0.2

1.  Bug Fix for HKEY_CLASSES_ROOT\DotnetScriptFiles\Shell\open\Command value.
changed 
E:\My Documents\My Articles\Dot Net Script\Source\DotNetScriptEngine\bin\Release\DotNetScriptEngine.exe %1
to
"E:\My Documents\My Articles\Dot Net Script\Source\DotNetScriptEngine\bin\Release\DotNetScriptEngine.exe" " %1"


2.  Added an optional entryPoint attribute to the language element in the dnml format.  
This would allow the user to specify an assembly entry point other than a method called "Main".  
If the entryPoint attribute is filled out with a method name, that method will become the entry point.
If the entryPoint attribute is missing or empty, then "Main" will become the assembly entry point.

The language element can be defined in one of the following three ways.
<language name="C#" />						Main()  will be the entry method to the assembly
<language name="C#" entryPoint"" />			Main()  will be the entry method to the assembly
<language name="C#" entryPoint"Stuff" />	Stuff() will be the entry method to the assembly


3.  Optional Wait For User Action: this is another new feature that lets you define in the .dnml file if you want the 
console window to remain open after the script is finished running.  The waitForUserAction element is an optional element.
If it is not included in the .dnml file, then the window will remain open.  The value attribute can hold the values
of 'true' or 'false'.  If true the window will remain open.  If false the console window will close immediately  after
the script if finished running.  This would allow you to chain different script files together into one batch file.
Possible ways to use this element.  
--nothing--								Console window will remain open after script has run
<waitForUserAction value="true"/>		Console window will remain open after script has run
<waitForUserAction value="True"/>		Console window will remain open after script has run
<waitForUserAction value="TRUE"/>		Console window will remain open after script has run
<waitForUserAction value="false"/>		Console window will close after script has run
<waitForUserAction value="False"/>		Console window will close after script has run
<waitForUserAction value="FALSE"/>		Console window will close after script has run


4.  Return an int from script to command prompt, cmd or batch file.  There are now two different ways to define
the return value for the entry method of the script.  You can define it as void, or you can define it as int. 
If you use void, then nothing will be returned from the script.  If you return an int, then the script engine will
also return that int to the calling process.

Example of two different dnml script entry methods:

//The script engine will return nothing when this script is called.
public static void Main()
{
	//...do stuff
	return;
}

//The script engine will return a 5 when this script is called.
public static int Main()
{
	//...do stuff
	return 5;
}

//The script engine will return nothing when this script is called.
Public Shared Sub Main()
	'...do stuff
	return
End Sub
	
//The script engine will return a 5 when this script is called.
Public Shared Function Main() as Integer
	'...do stuff
	return 5
End Function


Version 1.0.0.1
Initial rollout of Dot Net Script

