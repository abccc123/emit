﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
/******
Assign       ,generate entity, total 10000 rows, time: 3
Emit         ,generate entity, total 10000 rows, time: 7
Expression   ,generate entity, total 10000 rows, time: 38
Reflection   ,generate entity, total 10000 rows, time: 39
Delegate     ,generate entity, total 10000 rows, time: 137

 * ****/
namespace TEmit
{
    public class ToEntityByReflection<T>
    {
        public List<T> GetEntity(DataTable dt)
        {
            List<T> lst = new List<T>();
            Type type = typeof(T);
            foreach (DataRow dr in dt.Rows)
            {
                T model = (T)Activator.CreateInstance(typeof(T));
                foreach (PropertyInfo pi in type.GetProperties())
                {
                    pi.SetValue(model, Convert.ChangeType(dr[pi.Name], pi.PropertyType), null);
                }
                lst.Add(model);
            }
            return lst;
        }
    }
}
