﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TEmit
{
    public class DataTableData
    {
        public static DataTable GetCustomer(int _rows)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Name", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Age", typeof(System.String)));
            dt.Columns.Add(new DataColumn("Sex", typeof(System.String)));

            for (int i = 1; i <= _rows; i++)
            {
                DataRow dr = dt.NewRow();
                dr["Name"] = "Andy" + i.ToString();
                dr["Age"] = i.ToString();
                dr["Sex"] = "Male" + i.ToString();

                dt.Rows.Add(dr);
            }
            
            return dt;

        }
    }
}
