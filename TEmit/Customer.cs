﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEmit
{
    public class Customer
    {
        public string Name { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
    }
}
