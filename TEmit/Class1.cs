/* ============================================================================== 
2  * NET Framework�?.0.30319.18408
3  * �?�?者：$projectname$.qd01.zhouwei 
4  * 创建日期�?014/9/28 10:29:16  
5  * 功能说明�?6  * 引用说明：http://www.cnblogs.com/gaochundong/archive/2013/06/01/csharp_emit_create_constructor_properties.html
* ==============================================================================*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Reflection;
using System.Reflection.Emit;
//using System.Web.Caching;
using System.Web;

namespace TEmit
{
    public class Class2
    {
        public static List<T> ToList<T>(DataTable dt)
        {
            List<T> list = new List<T>();
            if (dt == null) return list;
            DataTableEntityBuilder<T> eblist = DataTableEntityBuilder<T>.CreateBuilder(dt.Rows[0]);
            foreach (DataRow info in dt.Rows)
                list.Add(eblist.Build(info));
            dt.Dispose();
            dt = null;
            return list;
        }

        public class DataTableEntityBuilder<T>
        {
            private static readonly MethodInfo getValueMethod = typeof(DataRow).GetMethod("get_Item", new Type[] { typeof(int) });
            private static readonly MethodInfo isDBNullMethod = typeof(DataRow).GetMethod("IsNull", new Type[] { typeof(int) });
            private delegate T Load(DataRow dataRecord);

            private Load handler;
            private DataTableEntityBuilder() { }

            public T Build(DataRow dataRecord)
            {
                return handler(dataRecord);
            }



            public static DataTableEntityBuilder<T> CreateBuilder(DataRow dataRow)
            {
                DataTableEntityBuilder<T> dynamicBuilder = new DataTableEntityBuilder<T>();
                DynamicMethod method = new DynamicMethod("DynamicCreateEntity", typeof(T), new Type[] { typeof(DataRow) }, typeof(T), true);
                ILGenerator generator = method.GetILGenerator();
                LocalBuilder result = generator.DeclareLocal(typeof(T));//表示方法或构造函数内的局部变�?                generator.Emit(OpCodes.Newobj, typeof(T).GetConstructor(Type.EmptyTypes));//实例�?                generator.Emit(OpCodes.Stloc, result);//从计算堆栈的顶部弹出当前值并将其存储到指定索引处的局部变量列表中�?
                for (int index = 0; index < dataRow.ItemArray.Length; index++)
                {
                    PropertyInfo propertyInfo = typeof(T).GetProperty(dataRow.Table.Columns[index].ColumnName);
                    Label endIfLabel = generator.DefineLabel();
                    if (propertyInfo != null && propertyInfo.GetSetMethod() != null)
                    {
                        generator.Emit(OpCodes.Ldarg_0);//将索引为 0 的参数加载到计算堆栈上�?                        generator.Emit(OpCodes.Ldc_I4, index);//将所提供�?int32 类型的值作�?int32 推送到计算堆栈上�?
                        generator.Emit(OpCodes.Callvirt, isDBNullMethod);//IsNull  对对象调用后期绑定方法，并且将返回值推送到计算堆栈�?                        generator.Emit(OpCodes.Brtrue, endIfLabel);//如果 value �?true、非空或非零，则将控制转移到目标指令�?                        generator.Emit(OpCodes.Ldloc, result);//指定索引处的局部变量加载到计算堆栈上�?
                        generator.Emit(OpCodes.Ldarg_0);
                        generator.Emit(OpCodes.Ldc_I4, index);
                        generator.Emit(OpCodes.Callvirt, getValueMethod);//get_Item  对象调用后期绑定方法，并且将返回值推送到计算堆栈�?                        generator.Emit(OpCodes.Unbox_Any, propertyInfo.PropertyType);// 将指令中指定类型的已装箱的表示形式转换成未装箱形式�?
                        generator.Emit(OpCodes.Callvirt, propertyInfo.GetSetMethod());
                        generator.MarkLabel(endIfLabel);
                    }
                }
                generator.Emit(OpCodes.Ldloc, result);
                generator.Emit(OpCodes.Ret);
                dynamicBuilder.handler = (Load)method.CreateDelegate(typeof(Load));
                return dynamicBuilder;
            }
        }
    }
}

