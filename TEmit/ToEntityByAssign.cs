﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
/******
Assign       ,generate entity, total 10000 rows, time: 3
Emit         ,generate entity, total 10000 rows, time: 7
Expression   ,generate entity, total 10000 rows, time: 38
Reflection   ,generate entity, total 10000 rows, time: 39
Delegate     ,generate entity, total 10000 rows, time: 137
http://www.cnblogs.com/andyliu/archive/2012/03/17/2402865.html
 * ****/
namespace TEmit
{
   public class ToEntityByAssign
    {
       public List<Customer> GetEntity(DataTable dt)
       {
           List<Customer> lst=new List<Customer>();
           foreach (DataRow dr in dt.Rows)
           {
               Customer cus = new Customer();
               cus.Name = dr["Name"].ToString();
               cus.Age = dr["Age"].ToString();
               cus.Sex = dr["Sex"].ToString();
               lst.Add(cus);
           }
           return lst;
       }
    }
}
