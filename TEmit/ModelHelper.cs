﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
namespace TEmit
{
    /// <summary>
    /// Reflection  转换  效率不高
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ModelHelper<T> where T : new()
    {
        public static T ConvertModel(DataRow dr)
        {
            T t = new T();
            Type modelType = t.GetType();
            foreach (PropertyInfo p in modelType.GetProperties())
            {
                p.SetValue(t, GetDefaultValue(dr[p.Name], p.PropertyType), null);
            }
            return t;
        }
        /// <summary>
        /// DataTable 转换为List 集合
        /// </summary>
        /// <typeparam name="TResult">类型</typeparam>
        /// <param name="dt">DataTable</param>
        /// <returns></returns>
        public List<TResult> ToList<TResult>(DataTable dt) where TResult : class, new()
        {
            //创建一个属性的列表
            List<PropertyInfo> prlist = new List<PropertyInfo>();
            //获取TResult的类型实例  反射的入口
            Type t = typeof(TResult);
            //获得TResult 的所有的Public 属性 并找出TResult属性和DataTable的列名称相同的属性(PropertyInfo) 并加入到属性列表
            Array.ForEach<PropertyInfo>(t.GetProperties(), p => { if (dt.Columns.IndexOf(p.Name) != -1) prlist.Add(p); });
            //创建返回的集合
            List<TResult> oblist = new List<TResult>();

            foreach (DataRow row in dt.Rows)
            {
                //创建TResult的实例
                TResult ob = new TResult();
                //找到对应的数据  并赋值
                prlist.ForEach(p => { if (row[p.Name] != DBNull.Value) p.SetValue(ob, row[p.Name], null); });
                //放入到返回的集合中.
                oblist.Add(ob);
            }
            return oblist;
        }
        private static object GetDefaultValue(object obj, Type type)
        {
            if (obj == DBNull.Value)
            {
                obj = default(object);
            }
            else
            {
                obj = Convert.ChangeType(obj, type);
            }
            return obj;
        }

    }
}
