//////////////////////////////////////////////////////////////////////
// Part of NetGenerator sourcecode. See version information
//////////////////////////////////////////////////////////////////////
// COPYRIGHTS:
// Copyright (c)2002 Solutions Design. All rights reserved.
// 
// Released under the following license: (BSD2)
// -------------------------------------------
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met: 
//
// 1) Redistributions of source code must retain the above copyright notice, this list of 
//    conditions and the following disclaimer. 
// 2) Redistributions in binary form must reproduce the above copyright notice, this list of 
//    conditions and the following disclaimer in the documentation and/or other materials 
//    provided with the distribution. 
// 
// THIS SOFTWARE IS PROVIDED BY SOLUTIONS DESIGN ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, 
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL SOLUTIONS DESIGN OR CONTRIBUTORS BE LIABLE FOR 
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
// USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
//
// The views and conclusions contained in the software and documentation are those of the authors 
// and should not be interpreted as representing official policies, either expressed or implied, 
// of Solutions Design. 
//
//////////////////////////////////////////////////////////////////////
// Contributers to the code:
//		- Frans Bouma [FB]
//////////////////////////////////////////////////////////////////////

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ICSharpCode.Core
{
    /// <summary>
	/// Purpose: general exception viewer.
	/// </summary>
	public class ExceptionDialog : System.Windows.Forms.Form
	{
		#region Class Member Declaration
        private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnClose;
        private Exception m_exException;
        private string m_message = string.Empty;
        private LinkLabel showDetails;
        private RichTextBox details;
        private RichTextBox messagebox;
		private System.ComponentModel.Container components = null;
		#endregion

		public ExceptionDialog()
		{
			InitializeComponent();
		}

		public ExceptionDialog(Exception exException, string message)
		{
			InitializeComponent();
			m_exException = exException;
            m_message = message;
			ViewExceptionInWindow();
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		
		/// <summary>
		/// Purpose: will load the private membervariable m_kexException's data
		/// into the windows' controls and thus view it. Set m_kexException with the
		/// property kexException.
		/// </summary>
        public void ViewExceptionInWindow()
        {
            // Error Message
            string message = this.m_message;
            if (message == null || message.Length == 0)
            {
                if (m_exException == null)
                {
                    message = "No exception loaded into window: can't visualize exception";
                }
                else
                {
                    message = m_exException.Message;
                }
            }

            messagebox.Text = message;

            // details
            Exception ex = m_exException;
            while (ex != null)
            {
                details.SelectionColor = Color.Red;
                details.SelectedText = ex.Message;
                details.SelectedText = Environment.NewLine;

                details.SelectionColor = Color.Blue;
                details.SelectedText = ex.StackTrace.Replace(Environment.NewLine, "\t" + Environment.NewLine);

                ex = ex.InnerException;
                if (ex != null)
                {
                    details.SelectedText = Environment.NewLine;
                }
            }

            HideDetails();
        }


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExceptionDialog));
            this.label1 = new System.Windows.Forms.Label();
            this.details = new System.Windows.Forms.RichTextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.showDetails = new System.Windows.Forms.LinkLabel();
            this.messagebox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 15);
            this.label1.TabIndex = 0;
            // 
            // details
            // 
            this.details.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.details.Location = new System.Drawing.Point(10, 118);
            this.details.Name = "details";
            this.details.Size = new System.Drawing.Size(631, 382);
            this.details.TabIndex = 5;
            this.details.Text = "";
            this.details.WordWrap = false;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnClose.Location = new System.Drawing.Point(296, 518);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(58, 26);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // showDetails
            // 
            this.showDetails.AutoSize = true;
            this.showDetails.Location = new System.Drawing.Point(12, 87);
            this.showDetails.Name = "showDetails";
            this.showDetails.Size = new System.Drawing.Size(77, 12);
            this.showDetails.TabIndex = 6;
            this.showDetails.TabStop = true;
            this.showDetails.Text = "Hide Details";
            this.showDetails.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.showDetails_LinkClicked);
            // 
            // messagebox
            // 
            this.messagebox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.messagebox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.messagebox.Location = new System.Drawing.Point(15, 11);
            this.messagebox.Name = "messagebox";
            this.messagebox.ReadOnly = true;
            this.messagebox.Size = new System.Drawing.Size(632, 73);
            this.messagebox.TabIndex = 7;
            this.messagebox.Text = "";
            // 
            // ExceptionDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
            this.ClientSize = new System.Drawing.Size(661, 550);
            this.Controls.Add(this.details);
            this.Controls.Add(this.messagebox);
            this.Controls.Add(this.showDetails);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExceptionDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Application Error";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
		
		/// <summary>
		/// Purpose: sets the membervariable m_exException which is visualized
		/// in the windows controls using ViewExceptionInWindow()
		/// </summary>
		public Exception Exception
		{
			set
			{
				m_exException = value;
			}
		}

        private void showDetails_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ToggleDetails();
        }

        void HideDetails()
        {
            this.showDetails.Text = "Show Details";
            this.details.Visible = false;
            this.Height = this.showDetails.Bottom + 57;
            this.Refresh();
        }

        void ShowDetails()
        {
            this.showDetails.Text = "Hide Details";
            this.details.Visible = true;
            this.Height = this.showDetails.Bottom + 300;
            this.Refresh();
        }

        void ToggleDetails()
        {
            if (!this.details.Visible)
            {
                ShowDetails();
            }
            else
            {
                HideDetails();
            }
        }

        public static void ShowException(IWin32Window owner, Exception ex, string message)
        {
            ExceptionDialog dlg = new ExceptionDialog(ex, message);
            dlg.ShowDialog(owner);
        }
	}
}
