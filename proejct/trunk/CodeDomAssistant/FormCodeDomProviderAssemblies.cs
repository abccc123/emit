using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Reflection;
using System.Reflection.Emit;
using System.IO;

namespace CodeDomAssistant
{
	/// <summary>
	/// Summary description for frmTableList.
	/// </summary>
	public class FormCodeDomProviderAssemblies : System.Windows.Forms.Form
    {
        System.Type subclass = typeof(System.Object);

		private System.Collections.Hashtable assemblies = new Hashtable();
        private ToolStrip toolStrip1;
        private ToolStripButton toolStripButtonAssembly;
        private DataGridView assemblyDataGridView;
        private Button buttonOk;
        private Button buttonCancel;
        Progress progress = new Progress();
        private System.Windows.Forms.Label labelProgress;
        private ToolStripButton toolStripButtonRefresh;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormCodeDomProviderAssemblies()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

            progress.NotifyProgress += new NotifyProgessHandler(progress_NotifyProgress);
		}

        void progress_NotifyProgress(Progress progress)
        {
            this.labelProgress.Text = progress.Message;
            this.labelProgress.Update();
        }

        public void LoadAssemblies(bool refreshcache)
        {
            // Get Current Settings
            DataSet assemblydata = DynProvider.GetAssemblyInfo();

            // Merge GAC Assemblies
            DynProvider.MergeGAC(assemblydata, refreshcache, progress);

            // Refresh Local Assemblies
            if (refreshcache)
            {
                DataRow[] localdlls = assemblydata.Tables[0].Select("IsGAC = 0");
                foreach (DataRow localdll in localdlls)
                {
                    DynProvider.MergeLocal(assemblydata, localdll["Path"].ToString(), progress);
                }
            }

            progress.Message = string.Empty;
            progress.Notify();

            DataView view = new DataView(assemblydata.Tables[0]);
            view.RowFilter = "HasProvider = 1";

            this.assemblyDataGridView.AutoGenerateColumns = false;
            this.assemblyDataGridView.DataSource = view;

            DataGridViewCheckBoxColumn c1 = new DataGridViewCheckBoxColumn();
            c1.DataPropertyName = "Load";
            c1.HeaderText = "Load";

            DataGridViewTextBoxColumn c2 = new DataGridViewTextBoxColumn();
            c2.DataPropertyName = "Name";
            c2.HeaderText = "Name";
            c2.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            c2.ReadOnly = false;

            this.assemblyDataGridView.AdvancedCellBorderStyle.All = DataGridViewAdvancedCellBorderStyle.None;

            this.assemblyDataGridView.Columns.Clear();
            this.assemblyDataGridView.Columns.Add(c1);
            this.assemblyDataGridView.Columns.Add(c2);

            this.assemblyDataGridView.AllowUserToResizeRows = false;
        }


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCodeDomProviderAssemblies));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonAssembly = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRefresh = new System.Windows.Forms.ToolStripButton();
            this.assemblyDataGridView = new System.Windows.Forms.DataGridView();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelProgress = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.assemblyDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonAssembly,
            this.toolStripButtonRefresh});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(790, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonAssembly
            // 
            this.toolStripButtonAssembly.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAssembly.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAssembly.Name = "toolStripButtonAssembly";
            this.toolStripButtonAssembly.Size = new System.Drawing.Size(55, 22);
            this.toolStripButtonAssembly.Text = "Browse";
            this.toolStripButtonAssembly.ToolTipText = "Browse For CodeDomProvider Assembly";
            this.toolStripButtonAssembly.Click += new System.EventHandler(this.toolStripButtonAssembly_Click);
            // 
            // toolStripButtonRefresh
            // 
            this.toolStripButtonRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonRefresh.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRefresh.Image")));
            this.toolStripButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRefresh.Name = "toolStripButtonRefresh";
            this.toolStripButtonRefresh.Size = new System.Drawing.Size(56, 22);
            this.toolStripButtonRefresh.Text = "Refresh";
            this.toolStripButtonRefresh.ToolTipText = "Refresh CodeDomProvider List";
            this.toolStripButtonRefresh.Click += new System.EventHandler(this.toolStripButtonRefresh_Click);
            // 
            // assemblyDataGridView
            // 
            this.assemblyDataGridView.AllowUserToAddRows = false;
            this.assemblyDataGridView.AllowUserToDeleteRows = false;
            this.assemblyDataGridView.AllowUserToOrderColumns = true;
            this.assemblyDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.assemblyDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.assemblyDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.assemblyDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.assemblyDataGridView.Location = new System.Drawing.Point(0, 23);
            this.assemblyDataGridView.Name = "assemblyDataGridView";
            this.assemblyDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.assemblyDataGridView.RowTemplate.Height = 24;
            this.assemblyDataGridView.Size = new System.Drawing.Size(790, 440);
            this.assemblyDataGridView.TabIndex = 2;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(524, 476);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(110, 34);
            this.buttonOk.TabIndex = 3;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(656, 476);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(110, 34);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelProgress
            // 
            this.labelProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelProgress.AutoSize = true;
            this.labelProgress.Location = new System.Drawing.Point(9, 473);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(0, 12);
            this.labelProgress.TabIndex = 5;
            // 
            // FormCodeDomProviderAssemblies
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
            this.ClientSize = new System.Drawing.Size(790, 524);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.assemblyDataGridView);
            this.Controls.Add(this.toolStrip1);
            this.Name = "FormCodeDomProviderAssemblies";
            this.ShowInTaskbar = false;
            this.Text = "CodeDomProvider Assemblies";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.assemblyDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        private void toolStripButtonAssembly_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.FileDialog fileDialog = new System.Windows.Forms.OpenFileDialog();
                fileDialog.DefaultExt = ".dll";
                fileDialog.Title = "Open CodeDomProvider DLL";
                fileDialog.SupportMultiDottedExtensions = true;
                fileDialog.Filter = "CodeDomProvider DLL (*.dll)|*.dll|All files (*.*)|*.*";

                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    foreach (string assemblyfile in fileDialog.FileNames)
                    {
                        DynProvider.MergeLocal(((DataView)this.assemblyDataGridView.DataSource).Table.DataSet, assemblyfile, null);
                    }
                }
            }
            catch (Exception ex)
            {
                ICSharpCode.Core.ExceptionDialog dlg = new ICSharpCode.Core.ExceptionDialog(ex, "Error Generating CodeDom");
                dlg.ShowDialog();
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (((DataView)this.assemblyDataGridView.DataSource).Table.DataSet.HasChanges())
                {
                    DynProvider.SaveAssemblyInfo(((DataView)this.assemblyDataGridView.DataSource).Table.DataSet);
                }

                this.DialogResult = DialogResult.OK;

                this.Close();
            }
            catch (Exception ex)
            {
                ICSharpCode.Core.ExceptionDialog dlg = new ICSharpCode.Core.ExceptionDialog(ex, "Error Generating CodeDom");
                dlg.ShowDialog();
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButtonRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                LoadAssemblies(true);
            }
            catch (Exception ex)
            {
                ICSharpCode.Core.ExceptionDialog dlg = new ICSharpCode.Core.ExceptionDialog(ex, "Error Generating CodeDom");
                dlg.ShowDialog();
            }
        }
	}
}
