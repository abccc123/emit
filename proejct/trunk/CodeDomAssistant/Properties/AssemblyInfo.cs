﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CodeDomAssistant")]
[assembly: AssemblyDescription("Generating CodeDom Code By Parsing C# or VB 使用C#或者VB生成CodeDom代码")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("raygilbert  & MysticBoy")]
[assembly: AssemblyProduct("CodeDomAssistant")]
[assembly: AssemblyCopyright("Copyright © raygilbert  & MysticBoy 2007-2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f1c81b46-6069-4bd4-a54e-7e9db66706fe")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.*")]
 
