using System;
using System.Collections.Generic;
using System.Text;

namespace CodeDomAssistant
{
    public delegate void NotifyProgessHandler(Progress progress);

    public class Progress
    {
        int progresscount = 0;
        int maxprogresscount = 0;
        string message = string.Empty;

        public event NotifyProgessHandler NotifyProgress;

        public Progress()
        {
        }

        public int ProgressCount
        {
            get { return progresscount; }
            set { progresscount = value; }
        }

        public int MaxProgressCount
        {
            get { return maxprogresscount; }
            set { maxprogresscount = value; }
        }

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public void Notify()
        {
            if (NotifyProgress != null)
            {
                NotifyProgress(this);
            }
        }
    }
}
