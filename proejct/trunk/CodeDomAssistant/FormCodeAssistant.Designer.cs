namespace CodeDomAssistant
{
    partial class FormCodeAssistant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabInputCode = new System.Windows.Forms.TabPage();
            this.scintillaInput = new ScintillaNet.Scintilla();
            this.tabOutputCode = new System.Windows.Forms.TabPage();
            this.scintillaOutput = new ScintillaNet.Scintilla();
            this.tabTestCodeDom = new System.Windows.Forms.TabPage();
            this.scintillaTestCodeDom = new ScintillaNet.Scintilla();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveOutputStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.codeDomProvidersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.radioVB = new System.Windows.Forms.RadioButton();
            this.radioCSharp = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.cbOutput = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonTestCodeDom = new System.Windows.Forms.Button();
            this.cbCodeDom = new System.Windows.Forms.ComboBox();
            this.tabControl1.SuspendLayout();
            this.tabInputCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scintillaInput)).BeginInit();
            this.tabOutputCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scintillaOutput)).BeginInit();
            this.tabTestCodeDom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scintillaTestCodeDom)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabInputCode);
            this.tabControl1.Controls.Add(this.tabOutputCode);
            this.tabControl1.Controls.Add(this.tabTestCodeDom);
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(744, 418);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabInputCode
            // 
            this.tabInputCode.Controls.Add(this.scintillaInput);
            this.tabInputCode.Location = new System.Drawing.Point(4, 25);
            this.tabInputCode.Name = "tabInputCode";
            this.tabInputCode.Padding = new System.Windows.Forms.Padding(3);
            this.tabInputCode.Size = new System.Drawing.Size(736, 389);
            this.tabInputCode.TabIndex = 0;
            this.tabInputCode.Text = "Input Code";
            this.tabInputCode.UseVisualStyleBackColor = true;
            // 
            // scintillaInput
            // 
            this.scintillaInput.Caret.Style = ScintillaNet.CaretStyle.Invisible;
            this.scintillaInput.CurrentPos = 0;
            this.scintillaInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scintillaInput.LineWrap.PositionCacheSize = 0;
            this.scintillaInput.Location = new System.Drawing.Point(3, 3);
            this.scintillaInput.Name = "scintillaInput";
            this.scintillaInput.Printing.PageSettings.Color = false;
            this.scintillaInput.Size = new System.Drawing.Size(730, 383);
            this.scintillaInput.TabIndex = 0;
            // 
            // tabOutputCode
            // 
            this.tabOutputCode.Controls.Add(this.scintillaOutput);
            this.tabOutputCode.Location = new System.Drawing.Point(4, 25);
            this.tabOutputCode.Name = "tabOutputCode";
            this.tabOutputCode.Padding = new System.Windows.Forms.Padding(3);
            this.tabOutputCode.Size = new System.Drawing.Size(736, 389);
            this.tabOutputCode.TabIndex = 1;
            this.tabOutputCode.Text = "Output Code";
            this.tabOutputCode.UseVisualStyleBackColor = true;
            // 
            // scintillaOutput
            // 
            this.scintillaOutput.Caret.Style = ScintillaNet.CaretStyle.Invisible;
            this.scintillaOutput.CurrentPos = 0;
            this.scintillaOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scintillaOutput.LineWrap.PositionCacheSize = 0;
            this.scintillaOutput.Location = new System.Drawing.Point(3, 3);
            this.scintillaOutput.Name = "scintillaOutput";
            this.scintillaOutput.Printing.PageSettings.Color = false;
            this.scintillaOutput.Size = new System.Drawing.Size(730, 383);
            this.scintillaOutput.TabIndex = 0;
            // 
            // tabTestCodeDom
            // 
            this.tabTestCodeDom.Controls.Add(this.scintillaTestCodeDom);
            this.tabTestCodeDom.Location = new System.Drawing.Point(4, 25);
            this.tabTestCodeDom.Name = "tabTestCodeDom";
            this.tabTestCodeDom.Size = new System.Drawing.Size(736, 389);
            this.tabTestCodeDom.TabIndex = 2;
            this.tabTestCodeDom.Text = "Test CodeDom";
            this.tabTestCodeDom.UseVisualStyleBackColor = true;
            // 
            // scintillaTestCodeDom
            // 
            this.scintillaTestCodeDom.Caret.Style = ScintillaNet.CaretStyle.Invisible;
            this.scintillaTestCodeDom.CurrentPos = 0;
            this.scintillaTestCodeDom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scintillaTestCodeDom.LineWrap.PositionCacheSize = 0;
            this.scintillaTestCodeDom.Location = new System.Drawing.Point(0, 0);
            this.scintillaTestCodeDom.Name = "scintillaTestCodeDom";
            this.scintillaTestCodeDom.Printing.PageSettings.Color = false;
            this.scintillaTestCodeDom.Size = new System.Drawing.Size(736, 389);
            this.scintillaTestCodeDom.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(744, 26);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveOutputStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(40, 22);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveOutputStripMenuItem
            // 
            this.saveOutputStripMenuItem.Name = "saveOutputStripMenuItem";
            this.saveOutputStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveOutputStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.saveOutputStripMenuItem.Text = "Save Output As";
            this.saveOutputStripMenuItem.Click += new System.EventHandler(this.saveOutputStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(239, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.X)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(43, 22);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // findToolStripMenuItem
            // 
            this.findToolStripMenuItem.Name = "findToolStripMenuItem";
            this.findToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.findToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.findToolStripMenuItem.Text = "Find and Replace";
            this.findToolStripMenuItem.Click += new System.EventHandler(this.findToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.codeDomProvidersToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(55, 22);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // codeDomProvidersToolStripMenuItem
            // 
            this.codeDomProvidersToolStripMenuItem.Name = "codeDomProvidersToolStripMenuItem";
            this.codeDomProvidersToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.codeDomProvidersToolStripMenuItem.Text = "CodeDomProviders...";
            this.codeDomProvidersToolStripMenuItem.Click += new System.EventHandler(this.codeDomProvidersToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.radioVB);
            this.groupBox1.Controls.Add(this.radioCSharp);
            this.groupBox1.Location = new System.Drawing.Point(13, 444);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(148, 66);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Input Parser";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(297, 18);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // radioVB
            // 
            this.radioVB.AutoSize = true;
            this.radioVB.Location = new System.Drawing.Point(83, 27);
            this.radioVB.Name = "radioVB";
            this.radioVB.Size = new System.Drawing.Size(47, 21);
            this.radioVB.TabIndex = 5;
            this.radioVB.Text = "VB";
            this.radioVB.UseVisualStyleBackColor = true;
            this.radioVB.CheckedChanged += new System.EventHandler(this.radioVB_CheckedChanged);
            // 
            // radioCSharp
            // 
            this.radioCSharp.AutoSize = true;
            this.radioCSharp.Checked = true;
            this.radioCSharp.Location = new System.Drawing.Point(6, 28);
            this.radioCSharp.Name = "radioCSharp";
            this.radioCSharp.Size = new System.Drawing.Size(46, 21);
            this.radioCSharp.TabIndex = 4;
            this.radioCSharp.TabStop = true;
            this.radioCSharp.Text = "C#";
            this.radioCSharp.UseVisualStyleBackColor = true;
            this.radioCSharp.CheckedChanged += new System.EventHandler(this.radioCSharp_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.buttonGenerate);
            this.groupBox2.Controls.Add(this.cbOutput);
            this.groupBox2.Location = new System.Drawing.Point(177, 444);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(561, 65);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Output Code";
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Location = new System.Drawing.Point(390, 18);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(154, 33);
            this.buttonGenerate.TabIndex = 1;
            this.buttonGenerate.Text = "Generate >>";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // cbOutput
            // 
            this.cbOutput.FormattingEnabled = true;
            this.cbOutput.Location = new System.Drawing.Point(18, 28);
            this.cbOutput.Name = "cbOutput";
            this.cbOutput.Size = new System.Drawing.Size(258, 24);
            this.cbOutput.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.buttonTestCodeDom);
            this.groupBox3.Controls.Add(this.cbCodeDom);
            this.groupBox3.Location = new System.Drawing.Point(177, 515);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(559, 64);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Test CodeDom";
            // 
            // buttonTestCodeDom
            // 
            this.buttonTestCodeDom.Enabled = false;
            this.buttonTestCodeDom.Location = new System.Drawing.Point(389, 18);
            this.buttonTestCodeDom.Name = "buttonTestCodeDom";
            this.buttonTestCodeDom.Size = new System.Drawing.Size(154, 33);
            this.buttonTestCodeDom.TabIndex = 3;
            this.buttonTestCodeDom.Text = "Test CodeDom >>";
            this.buttonTestCodeDom.UseVisualStyleBackColor = true;
            this.buttonTestCodeDom.Click += new System.EventHandler(this.buttonTestCodeDom_Click);
            // 
            // cbCodeDom
            // 
            this.cbCodeDom.FormattingEnabled = true;
            this.cbCodeDom.Location = new System.Drawing.Point(17, 28);
            this.cbCodeDom.Name = "cbCodeDom";
            this.cbCodeDom.Size = new System.Drawing.Size(258, 24);
            this.cbCodeDom.TabIndex = 2;
            // 
            // FormCodeAssistant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 588);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormCodeAssistant";
            this.Text = "CodeDom Assistant";
            this.tabControl1.ResumeLayout(false);
            this.tabInputCode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scintillaInput)).EndInit();
            this.tabOutputCode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scintillaOutput)).EndInit();
            this.tabTestCodeDom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scintillaTestCodeDom)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabInputCode;
        private System.Windows.Forms.TabPage tabOutputCode;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private ScintillaNet.Scintilla scintillaInput;
        private ScintillaNet.Scintilla scintillaOutput;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton radioVB;
        private System.Windows.Forms.RadioButton radioCSharp;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.ComboBox cbOutput;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveOutputStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonTestCodeDom;
        private System.Windows.Forms.ComboBox cbCodeDom;
        private System.Windows.Forms.TabPage tabTestCodeDom;
        private ScintillaNet.Scintilla scintillaTestCodeDom;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem codeDomProvidersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findToolStripMenuItem;
    }
}

